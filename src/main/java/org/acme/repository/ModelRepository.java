package org.acme.repository;

import io.quarkus.hibernate.orm.panache.PanacheRepository;
import org.acme.entity.Model;

import javax.enterprise.context.ApplicationScoped;

@ApplicationScoped
public class ModelRepository implements PanacheRepository<Model> {

}
