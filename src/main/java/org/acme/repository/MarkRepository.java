package org.acme.repository;

import io.quarkus.hibernate.orm.panache.PanacheRepository;
import org.acme.entity.Mark;

import javax.enterprise.context.ApplicationScoped;

@ApplicationScoped
public class MarkRepository implements PanacheRepository<Mark> {

}
