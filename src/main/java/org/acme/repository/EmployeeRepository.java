package org.acme.repository;

import io.quarkus.hibernate.orm.panache.PanacheRepository;
import org.acme.entity.Employee;

import javax.enterprise.context.ApplicationScoped;
import java.util.UUID;

@ApplicationScoped
public class EmployeeRepository implements PanacheRepository<Employee> {

    public Employee findById(UUID id) {
        return list("id = ?1", id).get(0);
    }



}
