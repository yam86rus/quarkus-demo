package org.acme.controller;

import org.acme.service.MarkService;

import javax.inject.Inject;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

@Path("/marks")
public class MarkController {

    private final MarkService markService;

    @Inject
    public MarkController(MarkService markService) {
        this.markService = markService;
    }

    @GET()
    @Produces(MediaType.APPLICATION_JSON)
    public Response getAllMarks(){
        return Response.ok(markService.findAll().list()).build();
    }
}
