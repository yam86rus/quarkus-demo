package org.acme.controller;

import org.acme.service.ModelService;

import javax.inject.Inject;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

@Path("/models")
public class ModelController {
    private final ModelService modelService;

    @Inject
    public ModelController(ModelService modelService) {
        this.modelService = modelService;
    }

    @GET
    @Path("/{id}")
    @Produces(MediaType.APPLICATION_JSON)
    public Response getAllMarks() {
        return Response.ok(modelService.findAll()).build();
    }
}
