package org.acme.controller;

import org.acme.entity.Employee;
import org.acme.service.EmployeeService;

import javax.inject.Inject;
import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.util.List;
import java.util.UUID;

@Path("/employees")
public class EmployeeController {
    private final EmployeeService employeeService;

    @Inject
    public EmployeeController(EmployeeService employeeService) {
        this.employeeService = employeeService;
    }

    @GET()
    @Produces(MediaType.APPLICATION_JSON)
    public Response getAllMarks(){
        return Response.ok(employeeService.findAll().list()).build();
    }

    @GET()
    @Path("/{id}")
    @Produces(MediaType.APPLICATION_JSON)
    public Employee getEmployee(@PathParam("id") UUID id){
        return employeeService.findById(id);
    }

    @POST()
    public void addEmployee(Employee employee){
        employeeService.addEmployee(employee);
    }

    @DELETE
    @Path("/{id}")
    public void deleteEmployee(UUID id){
        employeeService.deleteEmployee(id);
    }
}
