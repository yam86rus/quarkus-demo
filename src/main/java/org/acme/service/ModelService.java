package org.acme.service;

import org.acme.dto.ModelDTO;
import org.acme.mapper.ModelMapper;
import org.acme.repository.ModelRepository;

import javax.inject.Inject;
import javax.inject.Singleton;
import java.util.List;
import java.util.stream.Collectors;

@Singleton
public class ModelService {
    private final ModelRepository modelRepository;
    private final ModelMapper modelMapper;

    @Inject
    public ModelService(ModelRepository modelRepository, ModelMapper modelMapper) {
        this.modelRepository = modelRepository;
        this.modelMapper = modelMapper;
    }

    public List<ModelDTO>  findAll() {
        List<ModelDTO> collect = modelRepository.findAll().stream().map(modelMapper::toDTO).collect(Collectors.toList());
        return collect;
    }
}
