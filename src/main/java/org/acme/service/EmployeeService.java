package org.acme.service;

import io.quarkus.hibernate.orm.panache.PanacheQuery;
import org.acme.entity.Employee;
import org.acme.repository.EmployeeRepository;

import javax.inject.Inject;
import javax.inject.Singleton;
import javax.transaction.Transactional;
import javax.ws.rs.WebApplicationException;
import javax.ws.rs.core.Response;
import java.util.UUID;

@Singleton
public class EmployeeService {

    private final EmployeeRepository employeeRepository;

    @Inject
    public EmployeeService(EmployeeRepository employeeRepository) {
        this.employeeRepository = employeeRepository;
    }

    public PanacheQuery<Employee> findAll() {
        PanacheQuery<Employee> all = employeeRepository.findAll();
        return all;
    }

    public Employee findById(UUID id) {
        return employeeRepository.findById(id);
    }

    @Transactional
    public void addEmployee(Employee employee) {
        employeeRepository.persist(employee);
    }

    @Transactional
    public void deleteEmployee(UUID id) {
        Employee employee = findById(id);

        if (employee == null) {
            throw new WebApplicationException(Response.Status.NOT_FOUND);
        }
        employeeRepository.delete(employee);
    }
}
