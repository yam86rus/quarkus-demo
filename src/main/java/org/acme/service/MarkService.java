package org.acme.service;

import io.quarkus.hibernate.orm.panache.PanacheQuery;
import org.acme.entity.Mark;
import org.acme.repository.MarkRepository;

import javax.inject.Inject;
import javax.inject.Singleton;

@Singleton
public class MarkService {
    private final MarkRepository markRepository;

    @Inject
    public MarkService(MarkRepository markRepository) {
        this.markRepository = markRepository;
    }

    public PanacheQuery<Mark> findAll(){
        return markRepository.findAll();
    }
}
