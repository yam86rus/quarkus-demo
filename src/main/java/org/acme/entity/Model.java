package org.acme.entity;

import javax.persistence.*;
import java.util.Date;

@Entity
@Table(name = "model")
public class Model {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private Integer id;

    @Column(name = "name")
    private String name;

    @Column(name = "begin_create")
    private Date beginCreate;

    @Column(name = "finish_create")
    private Date finishCreate;

    public Model() {
    }

    public Model(Integer id, String name, Date beginCreate, Date finishCreate) {
        this.id = id;
        this.name = name;
        this.beginCreate = beginCreate;
        this.finishCreate = finishCreate;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Date getBeginCreate() {
        return beginCreate;
    }

    public void setBeginCreate(Date beginCreate) {
        this.beginCreate = beginCreate;
    }

    public Date getFinishCreate() {
        return finishCreate;
    }

    public void setFinishCreate(Date finishCreate) {
        this.finishCreate = finishCreate;
    }
}
