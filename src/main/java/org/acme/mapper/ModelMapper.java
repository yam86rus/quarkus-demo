package org.acme.mapper;

import org.acme.dto.ModelDTO;
import org.acme.entity.Model;
import org.mapstruct.Mapper;

@Mapper(componentModel = "cdi")
public interface ModelMapper {

    ModelDTO toDTO(Model model);
}
