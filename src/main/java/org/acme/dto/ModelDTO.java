package org.acme.dto;

import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.Date;

public class ModelDTO {
    @JsonProperty("name")
    private String name;

    @JsonProperty("begin_create")
    private Date beginCreate;

    @JsonProperty("finish_create")
    private Date finishCreate;

    public ModelDTO() {
    }

    public ModelDTO(String name, Date beginCreate, Date finishCreate) {
        this.name = name;
        this.beginCreate = beginCreate;
        this.finishCreate = finishCreate;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Date getBeginCreate() {
        return beginCreate;
    }

    public void setBeginCreate(Date beginCreate) {
        this.beginCreate = beginCreate;
    }

    public Date getFinishCreate() {
        return finishCreate;
    }

    public void setFinishCreate(Date finishCreate) {
        this.finishCreate = finishCreate;
    }
}
