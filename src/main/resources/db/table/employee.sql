----------------------------------------------------------
-- Таблица "Сотрудники"
----------------------------------------------------------

create table if not exists employee
(
    id            UUID PRIMARY KEY NOT NULL default gen_random_uuid(), -- ключ записи
    creation_date timestamp(6)     not null default now(),             -- дата создания записи
    first_name    varchar          null,                               -- имя
    last_name     varchar          null,                               -- фамилия
    city          varchar          null                                -- город
);

comment on table employee is 'Таблица "Сотрудники"';
comment on column employee.id is 'Ключ записи';
comment on column employee.first_name is 'Имя';
comment on column employee.last_name is 'Фамилия';
comment on column employee.city is 'Город';