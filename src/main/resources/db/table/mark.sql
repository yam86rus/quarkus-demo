----------------------------------------------------------
-- Таблица "Марка"
----------------------------------------------------------

create table if not exists mark
(
    id      serial primary key not null, -- ключ записи
    name    varchar            null,     -- наименование
    country varchar            null      -- страна
);

comment on table mark is 'Таблица "Марка"';
comment on column mark.id is 'Ключ записи';
comment on column mark.name is 'Наименование';
comment on column mark.country is 'Страна';