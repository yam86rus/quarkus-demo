insert into position (mark, model, create_date, mileage, price)
VALUES (1, 1, '2004-01-01', 125015, 1250000),
       (1, 1, '2005-01-01', 85000, 1500000),
       (1, 2, '2013-01-01', 45000, 1300000),
       (1, 2, '2015-01-01', 25000, 1800000),
       (1, 3, '2001-01-01', 25000, 1800000),
       (1, 3, '1998-01-01', 115000, 1800000),

       (2, 4, '2009-01-01', 95100, 750000),
       (2, 4, '2011-01-01', 124000, 950000),
       (2, 5, '2010-01-01', 163000, 1800000),
       (2, 5, '2012-01-01', 147000, 1650000),
       (2, 6, '2013-01-01', 125000, 1300000),
       (2, 6, '2014-01-01', 110000, 1450000);